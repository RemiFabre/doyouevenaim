# input lib
from pygame.locals import *
import pygame, string

class ConfigError(KeyError): pass

class Config:
    """ A utility for configuration """
    def __init__(self, options, *look_for):
        assertions = []
        for key in look_for:
            if key[0] in options.keys(): exec('self.'+key[0]+' = options[\''+key[0]+'\']')
            else: exec('self.'+key[0]+' = '+key[1])
            assertions.append(key[0])
        for key in options.keys():
            if key not in assertions: raise ConfigError(key+' not expected as option')

class Input:
    """ A text input for pygame apps """
    def __init__(self, **options):
        """ Options: x, y, font, color, restricted, maxlength, prompt """
        self.options = Config(options, ['x', '0'], ['y', '0'], ['font', 'pygame.font.Font(None, 32)'],
                              ['color', '(0,0,0)'], ['restricted', '\'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!"#$%&\\\'()*+,-./:;<=>?@[\]^_`{|}~\''],
                              ['maxlength', '-1'], ['prompt', '\'\''])
        self.x = self.options.x; self.y = self.options.y
        self.font = self.options.font
        self.color = self.options.color
        self.restricted = self.options.restricted
        self.maxlength = self.options.maxlength
        self.prompt = self.options.prompt; self.value = ''
        self.shifted = False

    def set_pos(self, x, y):
        """ Set the position to x, y """
        self.x = x
        self.y = y

    def set_font(self, font):
        """ Set the font for the input """
        self.font = font

    def draw(self, surface):
        """ Draw the text input to a surface """
        text = self.font.render(self.prompt+self.value, 1, self.color)
        surface.blit(text, (self.x, self.y))

    def erase(self, surface, background_color) :
        text = self.font.render(self.prompt+self.value, 1, self.color)
        text.fill(background_color)
        surface.blit(text, (self.x, self.y))

    def update(self, events):
        """ Update the input based on passed events """
        for event in events:
            if event.type == KEYUP:
                if event.key == K_LSHIFT or event.key == K_RSHIFT: self.shifted = False
            if event.type == KEYDOWN:
                if event.key == K_BACKSPACE: self.value = self.value[:-1]
                elif event.key == K_LSHIFT or event.key == K_RSHIFT: self.shifted = True
                elif event.key == K_SPACE: self.value += ' '
                # To handle qwerty issue in windows ...
                key = event.dict['unicode']
                if not self.shifted:
                    if key == 'a' and 'a' in self.restricted: self.value += 'a'
                    elif key == 'b' and 'b' in self.restricted: self.value += 'b'
                    elif key == 'c' and 'c' in self.restricted: self.value += 'c'
                    elif key == 'd' and 'd' in self.restricted: self.value += 'd'
                    elif key == 'e' and 'e' in self.restricted: self.value += 'e'
                    elif key == 'f' and 'f' in self.restricted: self.value += 'f'
                    elif key == 'g' and 'g' in self.restricted: self.value += 'g'
                    elif key == 'h' and 'h' in self.restricted: self.value += 'h'
                    elif key == 'i' and 'i' in self.restricted: self.value += 'i'
                    elif key == 'j' and 'j' in self.restricted: self.value += 'j'
                    elif key == 'k' and 'k' in self.restricted: self.value += 'k'
                    elif key == 'l' and 'l' in self.restricted: self.value += 'l'
                    elif key == 'm' and 'm' in self.restricted: self.value += 'm'
                    elif key == 'n' and 'n' in self.restricted: self.value += 'n'
                    elif key == 'o' and 'o' in self.restricted: self.value += 'o'
                    elif key == 'p' and 'p' in self.restricted: self.value += 'p'
                    elif key == 'q' and 'q' in self.restricted: self.value += 'q'
                    elif key == 'r' and 'r' in self.restricted: self.value += 'r'
                    elif key == 's' and 's' in self.restricted: self.value += 's'
                    elif key == 't' and 't' in self.restricted: self.value += 't'
                    elif key == 'u' and 'u' in self.restricted: self.value += 'u'
                    elif key == 'v' and 'v' in self.restricted: self.value += 'v'
                    elif key == 'w' and 'w' in self.restricted: self.value += 'w'
                    elif key == 'x' and 'x' in self.restricted: self.value += 'x'
                    elif key == 'y' and 'y' in self.restricted: self.value += 'y'
                    elif key == 'z' and 'z' in self.restricted: self.value += 'z'
                    elif key == '0' and '0' in self.restricted: self.value += '0'
                    elif key == '1' and '1' in self.restricted: self.value += '1'
                    elif key == '2' and '2' in self.restricted: self.value += '2'
                    elif key == '3' and '3' in self.restricted: self.value += '3'
                    elif key == '4' and '4' in self.restricted: self.value += '4'
                    elif key == '5' and '5' in self.restricted: self.value += '5'
                    elif key == '6' and '6' in self.restricted: self.value += '6'
                    elif key == '7' and '7' in self.restricted: self.value += '7'
                    elif key == '8' and '8' in self.restricted: self.value += '8'
                    elif key == '9' and '9' in self.restricted: self.value += '9'
                    elif event.key == K_BACKQUOTE and '`' in self.restricted: self.value += '`'
                    elif event.key == K_MINUS and '-' in self.restricted: self.value += '-'
                    elif event.key == K_EQUALS and '=' in self.restricted: self.value += '='
                    elif event.key == K_LEFTBRACKET and '[' in self.restricted: self.value += '['
                    elif event.key == K_RIGHTBRACKET and ']' in self.restricted: self.value += ']'
                    elif event.key == K_BACKSLASH and '\\' in self.restricted: self.value += '\\'
                    elif event.key == K_SEMICOLON and ';' in self.restricted: self.value += ';'
                    elif event.key == K_QUOTE and '\'' in self.restricted: self.value += '\''
                    elif event.key == K_COMMA and ',' in self.restricted: self.value += ','
                    elif event.key == K_PERIOD and '.' in self.restricted: self.value += '.'
                    elif event.key == K_SLASH and '/' in self.restricted: self.value += '/'
                elif self.shifted:
                    if key == 'A' and 'A' in self.restricted: self.value += 'A'
                    elif key == 'B' and 'B' in self.restricted: self.value += 'B'
                    elif key == 'C' and 'C' in self.restricted: self.value += 'C'
                    elif key == 'D' and 'D' in self.restricted: self.value += 'D'
                    elif key == 'E' and 'E' in self.restricted: self.value += 'E'
                    elif key == 'F' and 'F' in self.restricted: self.value += 'F'
                    elif key == 'G' and 'G' in self.restricted: self.value += 'G'
                    elif key == 'H' and 'H' in self.restricted: self.value += 'H'
                    elif key == 'I' and 'I' in self.restricted: self.value += 'I'
                    elif key == 'J' and 'J' in self.restricted: self.value += 'J'
                    elif key == 'K' and 'K' in self.restricted: self.value += 'K'
                    elif key == 'L' and 'L' in self.restricted: self.value += 'L'
                    elif key == 'M' and 'M' in self.restricted: self.value += 'M'
                    elif key == 'N' and 'N' in self.restricted: self.value += 'N'
                    elif key == 'O' and 'O' in self.restricted: self.value += 'O'
                    elif key == 'P' and 'P' in self.restricted: self.value += 'P'
                    elif key == 'Q' and 'Q' in self.restricted: self.value += 'Q'
                    elif key == 'R' and 'R' in self.restricted: self.value += 'R'
                    elif key == 'S' and 'S' in self.restricted: self.value += 'S'
                    elif key == 'T' and 'T' in self.restricted: self.value += 'T'
                    elif key == 'U' and 'U' in self.restricted: self.value += 'U'
                    elif key == 'V' and 'V' in self.restricted: self.value += 'V'
                    elif key == 'W' and 'W' in self.restricted: self.value += 'W'
                    elif key == 'X' and 'X' in self.restricted: self.value += 'X'
                    elif key == 'Y' and 'Y' in self.restricted: self.value += 'Y'
                    elif key == 'Z' and 'Z' in self.restricted: self.value += 'Z'
                    elif key == '0' and ')' in self.restricted: self.value += ')'
                    elif key == '1' and '!' in self.restricted: self.value += '!'
                    elif key == '2' and '@' in self.restricted: self.value += '@'
                    elif key == '3' and '#' in self.restricted: self.value += '#'
                    elif key == '4' and '$' in self.restricted: self.value += '$'
                    elif key == '5' and '%' in self.restricted: self.value += '%'
                    elif key == '6' and '^' in self.restricted: self.value += '^'
                    elif key == '7' and '&' in self.restricted: self.value += '&'
                    elif key == '8' and '*' in self.restricted: self.value += '*'
                    elif key == '9' and '(' in self.restricted: self.value += '('
                    elif event.key == K_BACKQUOTE and '~' in self.restricted: self.value += '~'
                    elif event.key == K_MINUS and '_' in self.restricted: self.value += '_'
                    elif event.key == K_EQUALS and '+' in self.restricted: self.value += '+'
                    elif event.key == K_LEFTBRACKET and '{' in self.restricted: self.value += '{'
                    elif event.key == K_RIGHTBRACKET and '}' in self.restricted: self.value += '}'
                    elif event.key == K_BACKSLASH and '|' in self.restricted: self.value += '|'
                    elif event.key == K_SEMICOLON and ':' in self.restricted: self.value += ':'
                    elif event.key == K_QUOTE and '"' in self.restricted: self.value += '"'
                    elif event.key == K_COMMA and '<' in self.restricted: self.value += '<'
                    elif event.key == K_PERIOD and '>' in self.restricted: self.value += '>'
                    elif event.key == K_SLASH and '?' in self.restricted: self.value += '?'

        if len(self.value) > self.maxlength and self.maxlength >= 0: self.value = self.value[:-1]
