"""Installation file """

from cx_Freeze import setup, Executable


# On appelle la fonction setup

setup(

    name = "doYouEvenAim",

    version = "0.1",

    description = "Aim practice",

    executables = [Executable("main.py")],

)
