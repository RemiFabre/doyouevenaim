#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random, math, pygame
import pygame.draw
from pygame.locals import *
import csv
import numpy
import matplotlib.pyplot as plt
import sys
import eztext
import os

#constants (we should put these in another file)
headshot_mode = 0
duration = 30
default_user_name = "unknown"
NUMSTARS = 150
INVISIBLE = (111,111,0)
WHITE = 255, 240, 200
BLACK = 20, 20, 40
MALUS = -2
RESTART_CODE = 1
QUIT_CODE = -1

# Small
WINSIZE_SMALL = [600, 350]
WINCENTER_SMALL = [WINSIZE_SMALL[0]/2, WINSIZE_SMALL[1]/2]
target_radius_small = 15
nb_lines_small = 5
color1_small = [200, 0, 0]
color2_small = [255, 255, 255]
headshot_offset_x = 300
headshot_offset_y = 30

# Big
WINSIZE_BIG = [1500, 840]
WINCENTER_BIG = [WINSIZE_BIG[0]/2, WINSIZE_BIG[1]/2]
target_radius_big = 50
nb_lines_big = 6
color1_big = [0, 0, 200]
color2_big = [255, 255, 255]

# Used
WINSIZE = WINSIZE_BIG
WINCENTER = WINCENTER_BIG
target_radius = target_radius_big
nb_lines = nb_lines_big
color1 = color1_big
color2 = color2_big

end_game_color = BLACK
user_name_pos = [10, 10]
text_pos = [10, 40]
time_pos = [10, 70]
leader_board_pos = [50, 200]
stats_pos = [0, WINSIZE[1] - 30]
final_score_pos = [50, WINCENTER[1]]

vaporeon = [153, 50, 204]
master = [245, 204, 176]
diamond = [0, 0, 0]
platinum = [229, 228, 226]
gold = [212, 175, 55]
silver = [192, 192, 192]
bronze = [91, 57, 30]
vomi = [115, 112, 0]

# The little points following the mouse
trace_on = True
smallPointRadius = 1
trace_color = [100, 0, 0]

# This is a list of pairs (represented by a tuple of size 2). The pair is : (minScore, color).
score_to_color = [(130, vaporeon), (120, master), (105, diamond), (90, platinum), (75, gold), (55, silver), (35, bronze), (-10000, BLACK)]
#score_to_color = [(12, diamond), (10, platinum), (8, gold), (5, silver), (3, bronze), (-10000, BLACK)]
# Font will be set at startup
font = 0
score_font = 0
small_font = 0


def create_target(radius, nbCircles, color1, color2) :
    surf = pygame.Surface([2*radius,2*radius])
    center = [radius, radius]
    # This color will be invisible
    surf.set_colorkey(INVISIBLE)
    surf.fill(INVISIBLE)
    if nbCircles < 1 :
        nbCircles = 1
    minRadius = radius/nbCircles
    for i in range(nbCircles) :
        r = round((nbCircles - i)*minRadius)
        if i%2 :
            pygame.draw.circle(surf, color1, center, r)
        else :
            pygame.draw.circle(surf, color2, center, r)

    return surf

def draw_small_point(surface, position) :
    surf = pygame.Surface([2*smallPointRadius, 2*smallPointRadius])
    # This color will be invisible
    surf.set_colorkey(INVISIBLE)
    surf.fill(INVISIBLE)
    pygame.draw.circle(surf, [150, 0, 0], [smallPointRadius, smallPointRadius], smallPointRadius)
    surface.blit(surf, [position[0] - smallPointRadius, position[1] - smallPointRadius])

def erase_list_of_points(surface, list_of_mouse_positions, color) :
    surf = pygame.Surface([2*smallPointRadius, 2*smallPointRadius])
    surf.fill(color)
    for p in list_of_mouse_positions :
        surface.blit(surf, [p[0] - smallPointRadius, p[1] - smallPointRadius])

def erase_and_draw_random_target(surface, target, old_pos, erase_color) :
    # Erasing the old target with a patch
    size = target.get_rect().width
    patch = pygame.Surface([size, size])
    patch.fill(erase_color)
    surface.blit(patch, old_pos)

    # Drawing the new target to a random position. (x, y) are the coordinates of the top left corner
    if (headshot_mode) :
        x = random.randint(headshot_offset_x, WINSIZE_SMALL[0] + 2*target_radius_small + headshot_offset_x)
        y = random.randint(headshot_offset_y, WINSIZE_SMALL[1] + 2*target_radius_small + headshot_offset_y)
        draw_headshot_rect(surface)
    else :
        x = random.randint(0, WINSIZE[0] - size)
        y = random.randint(0, WINSIZE[1] - size)
    surface.blit(target, [x, y])
    return [x, y]

def redraw_target(surface, target, current_pos) :
    if (headshot_mode) :
        draw_headshot_rect(surface)
    surface.blit(target, current_pos)

def draw_headshot_rect(surface) :
    pygame.draw.rect(surface, gold, (headshot_offset_x, headshot_offset_y, WINSIZE_SMALL[0] + headshot_offset_x + 2*target_radius_small, WINSIZE_SMALL[1] + headshot_offset_y + 2*target_radius_small), 2)

def get_score_from_distance(distance_to_center, target_radius) :
    #1/(1 + exp(-10*(x-0.5)))
    if target_radius < distance_to_center :
        # Shouldn't happen
        return MALUS
    else :
        distance_normalized = 1 - distance_to_center / target_radius
        # Sigmoid function
        sigmoid_score = 1/(1 + math.exp(-10*(distance_normalized - 0.5)))
        # We truncate the top extremum to make it clear that there is a limit (a very good shoot will become a perfect shot)
        if sigmoid_score > 0.95 :
            score = 1.0
        else :
            score = sigmoid_score
        return 1 + score

def init_star():
    "creates new star values"
    dir = random.randrange(100000)
    velmult = random.random()*.6+.4
    vel = [math.sin(dir) * velmult, math.cos(dir) * velmult]
    return vel, WINCENTER[:]


def initialize_stars():
    "creates a new starfield"
    stars = []
    for x in range(NUMSTARS):
        star = init_star()
        vel, pos = star
        steps = random.randint(0, WINCENTER[0])
        pos[0] = pos[0] + (vel[0] * steps)
        pos[1] = pos[1] + (vel[1] * steps)
        vel[0] = vel[0] * (steps * .09)
        vel[1] = vel[1] * (steps * .09)
        stars.append(star)
    move_stars(stars)
    return stars


def draw_stars(surface, stars, color):
    "used to draw (and clear) the stars"
    for vel, pos in stars:
        pos = (int(pos[0]), int(pos[1]))
        surface.set_at(pos, color)


def move_stars(stars):
    "animate the star values"
    for vel, pos in stars:
        pos[0] = pos[0] + vel[0]
        pos[1] = pos[1] + vel[1]
        if not 0 <= pos[0] <= WINSIZE[0] or not 0 <= pos[1] <= WINSIZE[1]:
            vel[:], pos[:] = init_star()
        else:
            vel[0] = vel[0] * 1.05
            vel[1] = vel[1] * 1.05

def color_replace(surface, find_color, replace_color):
    for x in range(surface.get_size()[0]):
        for y in range(surface.get_size()[1]):
            if surface.get_at([x, y]) == find_color:
                surface.set_at([x, y], replace_color)
    return surface

def calculate_efficiency(positions) :
    if len(positions) < 2 :
        print("Not enough points to calculate efficiency...")
        return 1
    x0 = positions[0][0]
    y0 = positions[0][1]
    x1 = positions[-1][0]
    y1 = positions[-1][1]
    ideal_distance = math.sqrt(math.pow(x0 - x1, 2) + math.pow(y0 - y1, 2))
    real_distance = 0
    for i in range(len(positions)) :
        # We're measuring a distance between a point and its previous point. The first point has no previous point
        if i == 0 :
            continue
        x0 = positions[i][0]
        y0 = positions[i][1]
        x1 = positions[i-1][0]
        y1 = positions[i-1][1]
        distance = math.sqrt(math.pow(x0 - x1, 2) + math.pow(y0 - y1, 2))
        real_distance = real_distance + distance

    if (real_distance == 0) :
        print ("Real distance is 0... 1 chance over ", WINSIZE[0]*WINSIZE[1])
        return 1
    efficiency = ideal_distance / float(real_distance)

    return efficiency, ideal_distance

def blit_leader_board(surface, best_scores, write_colors, background_color) :
    ''' To slow, abandoned method
    if (old_surface == None) :
        # The first time we create a surface fully transparent of the size of the screen
        old_surface = pygame.Surface((surface.get_rect().width, surface.get_rect().height))
        # This color will be invisible
        old_surface.set_colorkey(INVISIBLE)
        old_surface.fill(INVISIBLE)
    else :
        for c in write_colors :
            old_surface = color_replace(old_surface, c, background_color)
    '''
    # Brutal method, erasing a big portion of the screen
    temp_surface = pygame.Surface((surface.get_rect().width, surface.get_rect().height - leader_board_pos[1] + 40))
    temp_surface.fill(background_color)
    surface.blit(temp_surface, [leader_board_pos[0] + 10, leader_board_pos[1] + 40])

    max_entries = 7
    nb_entries = min(len(best_scores), max_entries)
    for i in range(max_entries) :
        if (i == 0) :
            color = write_colors[0]
        else :
            color = write_colors[1]
        if (i < nb_entries) :
            name_and_score = best_scores[i]
            text = font.render(str(i+1) + " " + "{0:.2f}".format(name_and_score[1]) + " points    " + str(name_and_score[0]), 1, color)
            surface.blit(text, [leader_board_pos[0] + 10, leader_board_pos[1] + 40 + 40*(i+1)])
        else :
            text = font.render(str(i+1) + " -", 1, color)
            surface.blit(text, [leader_board_pos[0] + 10, leader_board_pos[1] + 40 + 40*(i+1)])
    surface.blit(surface, [0, 0])

def start_screen(screen) :
    global headshot_mode
    clock = pygame.time.Clock()
    screen.fill(BLACK)

    user_name = default_user_name
    enter_position = [0, 0]
    headshot_position = [0, 30]

    text = small_font.render("Enter your user name and press Enter...", 1, WHITE)
    screen.blit(text, enter_position)

    text = small_font.render("Press Ctrl to toggle the headshot mode...", 1, WHITE)
    screen.blit(text, headshot_position)

    text_box = eztext.Input(maxlength=45, color=WHITE, prompt='Name : ')
    text_box.set_pos(20, 70)

    text = score_font.render("Leaderboard", 1, WHITE)
    screen.blit(text, leader_board_pos)

    # Pairs of [name, best score]
    best_scores = []
    # Getting the best scores for each user (normal mode) :
    i = 0
    dir_path = "."
    for filename in os.listdir(dir_path):
        i = i + 1
        if filename.endswith(".scores"):
            print ("Reading ", filename, "...")
            file_path = os.path.join(dir_path, filename)
            best_scores.append([filename[:-7], get_best_score(file_path)])
    best_scores = sorted(best_scores, key=lambda score: score[1], reverse=True)
    print(best_scores)

    best_scores_hs = []
    for filename in os.listdir(dir_path):
        i = i + 1
        if filename.endswith(".hsscores"):
            print ("Reading ", filename, "...")
            file_path = os.path.join(dir_path, filename)
            best_scores_hs.append([filename[:-9], get_best_score(file_path)])
    best_scores_hs = sorted(best_scores_hs, key=lambda score: score[1], reverse=True)

    print(best_scores_hs)

    blit_leader_board(screen, best_scores, [gold, WHITE], BLACK)

    small_target = create_target(target_radius_small, nb_lines_small, color1_small, color2_small)
    fake_small_target = create_target(target_radius_small, nb_lines_small, BLACK, BLACK)
    while True:
        score = 0
        events = pygame.event.get()
        # 'e' for 'event'
        for e in events :
            if e.type == QUIT :
                sys.exit()
            elif e.type == KEYDOWN:
                key = e.dict['unicode']
                if (e.key == K_RETURN) :
                    # Time to leave the start screen
                    name = text_box.value
                    if (len(name) > 0) :
                        user_name = name
                    return user_name
                elif (pygame.key.get_mods() & pygame.KMOD_CTRL) :
                    if (headshot_mode) :
                        headshot_mode = 0
                        screen.blit(fake_small_target, [headshot_position[0] + 500, headshot_position[1] - target_radius_small/2])
                        # Showing the normal leader board
                        blit_leader_board(screen, best_scores, [gold, WHITE], BLACK)
                    else :
                        headshot_mode = 1
                        screen.blit(small_target, [headshot_position[0] + 500, headshot_position[1] - target_radius_small/2])
                        # Showing the headshot leader board
                        blit_leader_board(screen, best_scores_hs, [gold, WHITE], BLACK)
            elif e.type == pygame.MOUSEMOTION:
                x, y = e.pos
                if (trace_on) :
                    draw_small_point(screen, [x, y])

        # Text box update
        text_box.erase(screen, BLACK)
        text_box.update(events)
        text_box.draw(screen)
        pygame.display.update()
        # That juicy 60 Hz :D
        clock.tick(60)

def play_a_game(screen, user_name) :
    global target_radius, nb_lines, color1, color2
    if (headshot_mode) :
        target_radius = target_radius_small
        nb_lines = nb_lines_small
        color1 = color1_small
        color2 = color2_small

    stars = initialize_stars()
    clock = pygame.time.Clock()
    # We start with the last color
    current_background_color = score_to_color[-1][1]
    previous_background_color = current_background_color
    screen.fill(current_background_color)
    target = create_target(target_radius, nb_lines, color1, color2)
    # Draw first target
    current_pos = [WINCENTER[0] - target_radius, WINCENTER[1] - target_radius]
    screen.blit(target, current_pos)

    text = small_font.render("Press s to see some nice stats...", 1, gold)
    screen.blit(text, stats_pos)

    list_of_efficiencies = []
    total_score = 0.0
    #main game loop
    done = 0
    startGame = False
    t = 0
    t0 = 0
    remaining_time = duration - t
    nb_hit = 0
    list_of_mouse_positions = []

    total_distance = 0

    while not done:
        if (previous_background_color != current_background_color) :
            # We're painting the background with a different color
            screen.fill(current_background_color)
            previous_background_color = current_background_color
            # We've just erased everything, we'll redraw the target
            redraw_target(screen, target, current_pos)
        draw_stars(screen, stars, current_background_color)
        move_stars(stars)
        draw_stars(screen, stars, WHITE)
        score = 0

        # 'e' for 'event'
        for e in pygame.event.get():
            if e.type == QUIT :
                sys.exit()
            elif e.type == KEYDOWN:
                key = e.dict['unicode']
                if (key == "s") :
                    # We'll show some nice stats
                    load_and_show_stats(user_name)
            elif e.type == MOUSEBUTTONDOWN and e.button == 1:
                if (startGame) :
                    # Calculating movement efficiency
                    efficiency, distance = calculate_efficiency(list_of_mouse_positions)
                    total_distance = total_distance + distance
                    list_of_efficiencies.append(efficiency)
                    # Erasing the list of positions
                    if trace_on :
                        erase_list_of_points(screen, list_of_mouse_positions, current_background_color)
                    list_of_mouse_positions = []
                # The current_pos is actually the top left corner, we want the center of the target so we must add the target radius to make it work
                distance_to_center = math.sqrt(pow(e.pos[0] - (current_pos[0] + target_radius), 2) + pow(e.pos[1] - (current_pos[1] + target_radius), 2))
                if (distance_to_center <= target_radius) :
                    # The shot is inside the target !
                    nb_hit = nb_hit + 1
                    if (headshot_mode) :
                        score = 2
                    else :
                        score = get_score_from_distance(distance_to_center, target_radius)
                    # Creating a new target
                    current_pos = erase_and_draw_random_target(screen, target, current_pos, current_background_color)
                    if (not(startGame)) :
                        # The game starts now !
                        startGame = True
                        t0 = pygame.time.get_ticks()
                        # Erasing the stats sentence
                        text.fill(current_background_color)
                        screen.blit(text, stats_pos)
                else :
                    # The shot missed !
                    score = MALUS
                WINCENTER[:] = list(e.pos)
            elif e.type == pygame.MOUSEMOTION:
                x, y = e.pos
                list_of_mouse_positions.append((x, y))
                if (trace_on and startGame) :
                    draw_small_point(screen, [x, y])

        try :
            # Erasing the previous text
            score_text.fill(current_background_color)
            screen.blit(score_text, text_pos)
            time_text.fill(current_background_color)
            screen.blit(time_text, time_pos)
        except Exception :
            print("Nothing to erase here")

        str_time = "{0:.2f}".format(remaining_time/1000.0)
        time_text = font.render("Time : " + str_time, 1, current_background_color)
        screen.blit(time_text, time_pos)

        # Updating values to be printed on the screen
        total_score = total_score + score
        # Checking if we reached a color checkpoint ...
        for s in score_to_color :
            if (total_score >= s[0]) :
                # We deserve a new color ! Will be updated next round
                current_background_color = s[1]
                break

        if (startGame) :
            t = pygame.time.get_ticks() - t0
        remaining_time = duration*1000 - t

        if remaining_time < 0 :
            break

        # Writing new text
        str_score = "{0:.2f}".format(total_score)
        score_text = font.render("Score : " + str_score, 1, (0,150,0))
        screen.blit(score_text, text_pos)

        str_time = "{0:.2f}".format(remaining_time/1000.0)
        time_text = font.render("Time : " + str_time, 1, (0,150,0))
        screen.blit(time_text, time_pos)

        name_text = font.render(user_name, 1, (0,150,0))
        screen.blit(name_text, user_name_pos)

        pygame.display.update()
        # That juicy 60 Hz :D
        clock.tick(60)

    #Calculating the average efficiency
    average_efficiency = 0
    for eff in list_of_efficiencies :
        average_efficiency = average_efficiency + eff
    if (len(list_of_efficiencies) < 1) :
        average_efficiency = 0
    else :
        average_efficiency = average_efficiency / float(len(list_of_efficiencies))
    return total_score, nb_hit, average_efficiency, total_distance

def end_screen(screen, user_name, total_score, nb_hit, average_efficiency, total_distance) :
    save_score(user_name, total_score, nb_hit/float(duration), average_efficiency, total_distance)

    clock = pygame.time.Clock()
    # We just finished a game, let's print the score to the user and give him some feedback on his performance
    screen.fill(end_game_color)
    str_score = "{0:.2f}".format(total_score)
    score_text = score_font.render("Your final score is : " + str_score, 1, (0,150,0))
    screen.blit(score_text, final_score_pos)

    str_nb_hit = str(nb_hit)
    nb_hit_text = small_font.render("Hits : " + str_nb_hit, 1, (0,150,0))
    screen.blit(nb_hit_text, [final_score_pos[0], final_score_pos[1] + 70])

    str_nb_hit_per_s = "{0:.2f}".format(nb_hit/float(duration))
    nb_hit_per_s_text = small_font.render("Hits/s : " + str_nb_hit_per_s, 1, (0,150,0))
    screen.blit(nb_hit_per_s_text, [final_score_pos[0], final_score_pos[1] + 90])

    # Max score for 1 hit is 2 points
    str_acc = "{0:.2f}".format(total_score/(2*nb_hit))
    acc_text = small_font.render("Accuracy : " + str_acc, 1, (0,150,0))
    screen.blit(acc_text, [final_score_pos[0], final_score_pos[1] + 110])

    str_eff = "{0:.2f}".format(average_efficiency)
    eff_text = small_font.render("Efficiency : " + str_eff, 1, (0,150,0))
    screen.blit(eff_text, [final_score_pos[0], final_score_pos[1] + 130])

    str_dist = "{0:.2f}".format(total_distance)
    dist_text = small_font.render("Total distance : " + str_dist, 1, (0,150,0))
    screen.blit(dist_text, [final_score_pos[0], final_score_pos[1] + 150])

    stats_text = small_font.render("Press s to see some nice stats...", 1, WHITE)
    screen.blit(stats_text, stats_pos)

    restart_text = small_font.render("Press enter to try again ...", 1, gold)
    screen.blit(restart_text, [0, 0])
    restart_text = small_font.render("Press q to leave", 1, WHITE)
    screen.blit(restart_text, [0, WINSIZE[1] - 50])
    done = False

    while not done:
        for e in pygame.event.get():
            if e.type == QUIT :
                return QUIT_CODE
            elif e.type == KEYDOWN:
                key = e.dict['unicode']
                if (e.key == K_RETURN) :
                    # We'll restart the game
                    return RESTART_CODE
                elif (key == "q") :
                    return QUIT_CODE
                elif (key == "s") :
                    # We'll show some nice stats
                    load_and_show_stats(user_name)
        pygame.display.update()
        # That juicy 60 Hz :D
        clock.tick(60)

def save_score(user_name, score, score_per_s,  average_efficiency, total_distance) :
    if (headshot_mode) :
        user_name = user_name + ".hsscores"
    else :
        user_name = user_name + ".scores"

    with open(user_name, 'a+', newline='\n') as csvfile:
        row_to_be_saved = ["{0:.2f}".format(score), "{0:.2f}".format(score_per_s), "{0:.2f}".format(average_efficiency), "{0:.2f}".format(total_distance)]
        print ("Saving score : ", row_to_be_saved)
        data_writer = csv.writer(csvfile, delimiter=' ')
        data_writer.writerow(row_to_be_saved)

def load_and_show_stats(user_name) :
    if (headshot_mode) :
        user_name = user_name + ".hsscores"
    else :
        user_name = user_name + ".scores"
    try :
        with open(user_name, 'r', newline='\n') as csvfile:
            rows = csv.reader(csvfile, delimiter=' ', quotechar='|')
            list_of_stats = []
            for r in rows :
                list_of_stats.append(r)
        if (len(list_of_stats) < 1) :
            raise Exception()
    except Exception :
        print("No stats for the user : '", user_name, "'")
        return
    plot_stats(list_of_stats)

def plot_stats(rows) :
    T = numpy.arange(0, len(rows), 1)
    score, hits_per_s, average_efficiency, total_distance = zip(*rows)

    plt.subplot(221)
    plt.plot(T, score, "-")
    plt.legend(['Score'])
    plt.grid(True)
    plt.subplot(222)
    plt.plot(T, hits_per_s, "-")
    plt.legend(['Hits/s'])
    plt.grid(True)
    plt.subplot(223)
    plt.plot(T, average_efficiency, "-")
    plt.legend(['Average Efficiency'])
    plt.grid(True)
    plt.subplot(224)
    plt.plot(T, total_distance, "-")
    plt.legend(['Total_distance'])
    plt.grid(True)
    plt.show(block=True)

def get_best_score(path) :
    max = 0
    try :
        with open(path, 'r', newline='\n') as csvfile:
            rows = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for r in rows :
                score = float(r[0])
                if (score > max) :
                    max = score
    except Exception as e:
        print("Exception : ", e)
        print("Error : failed to handle file : '", path, "'")
        return max
    return max

def main():
    #create our starfield
    random.seed()
    #initialize and prepare screen
    pygame.init()
    screen = pygame.display.set_mode(WINSIZE)
    pygame.display.set_caption('Do you even aim?')
    # Font init
    global font, score_font, small_font
    small_font = pygame.font.SysFont("monospace", 15)
    font = pygame.font.SysFont("monospace", 30)
    score_font = pygame.font.SysFont("monospace", 40)


    screen.fill(BLACK)

    user_name = start_screen(screen)

    screen.fill(BLACK)

    total_score, nb_hit, average_efficiency, total_distance = play_a_game(screen, user_name)

    while True :
        user_answer = end_screen(screen, user_name, total_score, nb_hit, average_efficiency, total_distance)
        if user_answer == RESTART_CODE :
            total_score, nb_hit, average_efficiency, total_distance = play_a_game(screen, user_name)
        else :
            # Leaving the game
            sys.exit()


# if python says run, then we should run
if __name__ == '__main__':
    #if(len(sys.argv[]) > 1 and sys.argv[1] == "-s") :
    main()




# One day :
'''
﻿Unranked : Prepuce (juif?) ignorant
B5: Justin Bieber follower
B4: Affligeante souillure ambulante
B3: Lamentable souillure ambulante
B2: Petite souillure ambulante
B1: Honorable souillure ambulante
Note : "When a brother beds a sister, the gods toss a coin."
A5: Consanguin Texan (pléonasme)
A4: Consanguin breton (Pléonasme)
A3: Consanguin ardéchois (croisé vache)
A2: Consanguin épargné
A1: Espoir parmis les consanguins
G5: Petit tocard affable
G4: Petit vermisseau
G3: Petit avorton
G2: Petit paysan
G1: Petit Guerrier
P5: Grand Guerrier
P4: Champion
P3: Maitre
P2: Légende
P1: Demi-Dieu
D5: Petit Dieu
D4: Dieu paien (au choix)
D3: Dieu Grec (au choix)
D2: Dieu Monotheiste (au choix)
D1+: GokuIsThatYou?
'''
